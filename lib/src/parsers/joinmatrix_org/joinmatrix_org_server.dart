import '../../models/homeserver.dart';

/// creates a [Homeserver] parsed from
/// [joinMatrix.org](https://joinMatrix.org/)'s raw data
class JoinMatrixOrgServer extends Homeserver {
  JoinMatrixOrgServer.fromJson(Map<String, Object?> json)
      : super(
          baseUrl: Uri.parse('https://${json['name']}'),
          aboutUrl: Uri.tryParse(json['homepage'] as String? ?? ''),
          jurisdiction: json['staff_jur'] as String?,
          rules: Uri.tryParse(json['rules'] as String? ?? ''),
          privacyPolicy: Uri.tryParse(json['privacy'] as String? ?? ''),
          registrationAllowed:
              json['using_vanilla_reg'] == true || json['reg_link'] != null,
          description: json['description'] as String?,
          registration: Uri.tryParse(json['reg_link'] as String? ?? ''),
          homeserverSoftware: '${json['software']} ${json['version']}',
        );
}
